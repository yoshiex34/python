# Base project format.
# Documentation https://github.com/raspberrypilearning/getting-started-with-minecraft-pi/blob/master/worksheet.md
from mcpi.minecraft import Minecraft
from mcpi import block

ip = "127.0.0.1"
mc = Minecraft.create(ip, 4711)
mc.player.setPos(0,0,0)
mc.setBlocks(-128,-3,-128,128,64,128,0)
#mc.setBlocks(10,10,10,20,20,20,1)
mc.setBlock(-8,-3,1,14)
mc.setBlock(-8,-2,1,15)
mc.setBlock(-8,-1,1,16)
mc.setBlock(-8,0,1,17)
mc.setBlock(-8,1,1,18)
mc.setBlock(-8,-1,0,16)
mc.setBlock(-8,-1,-1,16)
mc.setBlock(-8,-1,-2,16)
mc.setBlock(-8,0,-2,17)
mc.setBlock(-8,1,-2,18)
mc.setBlock(-8,-2,-2,15)
mc.setBlock(-8,-3,-2,14)
mc.setBlock(-8,-3,-4,12)
mc.setBlock(-8,-2,-4,12)
mc.setBlock(-8,0,-4,246)
mc.player.setPos(0,5  ,0)
